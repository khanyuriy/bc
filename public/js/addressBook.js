/// <reference path="../../typings/angularjs/angular.d.ts"/>
angular.module('addressBookApp', ['ngDialog'])
.factory('addressBookService', ['$http', function ($http) {
	var makeCall = function (phoneNumber, success, error) {
		$http.post('/asterisk/call', {phone: phoneNumber}).success(success).error(error);
	};
	
	var getCompanies = function (success, error) {
		$http.post('/companies', {}).success(success).error(error);
	};
	
	var sendEmail = function (to, subject, text, success, error) {
		$http.post('/email', {to: to, subject: subject, text: text}).success(success).error(error);
	}
	
	return {
		makeCall: makeCall,
		getCompanies: getCompanies,
		sendEmail: sendEmail
	};
}])
.controller('addressBookController', ['$scope', 'addressBookService', 'ngDialog',
	function ($scope, service, ngDialog) {
		
	function init() {
		$scope.isSearchInputFocused = true;
		$scope.orderProperty = 'name';
		loadCompanies();
	}
		 
	function loadCompanies() {
		$scope.isLoading = true;
		service.getCompanies(function (companies) {
			$scope.companies = companies;
			$scope.isLoading = false;
		}, function () {			
			$scope.isLoading = false;
		});
	}

	$scope.makeCall = function (addressBookEntry) {
		service.makeCall(addressBookEntry.phone, 
			function success(data) {
				console.log('call success'+JSON.stringify(data));
			}, 
			function error(data) {
				console.log('call error'+JSON.stringify(data));
			});
	};
	
	$scope.showSendEmailDialog = function (company) {
		var dialogScope = $scope.$new();
		dialogScope.recipient = company;
		
		openDialog(dialogScope);		
	};
	
	$scope.showSendToAllEmailDialog = function () {
		var dialogScope = $scope.$new();
		dialogScope.recipient = $scope.companies;
		
		openDialog(dialogScope);
	}
	
	function openDialog(dialogScope) {
		ngDialog.open({		
			scope: dialogScope,	
			template: '/emailDialogTemplate',
			controller: 'emailDialogController'			
		});
	}
	
	$scope.orderByName = function () {
		$scope.orderProperty = 'name';
	};
	
	$scope.orderByRoom = function () {
		$scope.orderProperty = 'room';
	};
	
	$scope.isOrderedByName = function () {
		return $scope.orderProperty == 'name';
	};
	
	$scope.isOrderedByRoom = function () {
		return $scope.orderProperty == 'room';
	};

	init();
 }])
 .controller('emailDialogController', ['$scope', 'addressBookService', function ($scope, service) {
	 
	 function init() {
		 $scope.isSending = false;
	 }
	 
	 $scope.getTo = function () {
		 if (angular.isArray($scope.recipient)) {
			 return 'Всем';
		 }
		 
		return $scope.recipient.name + ' <'+$scope.recipient.email+'>'; 
	 };
	 
	 $scope.sendEmail = function () {
		 
		 var recipient = formRecipient();
		 $scope.isSending = true;
		 service.sendEmail(recipient, $scope.subject, $scope.text, function (info) {
			 $scope.isSending = false;
			$scope.closeThisDialog();
			 
			console.log('send mail success');
		}, function (error) {
			$scope.isSending = false;
			console.log('send mail error');
		});
	 };
	 
	 $scope.isSendDisabled = function () {
		 return !$scope.subject || !$scope.text || $scope.isSending === true;
	 };
	 
	 function formRecipient() {
		 if (angular.isArray ($scope.recipient)) {
			 var result = '';
			 for (var i = 0; i<$scope.recipient.length; i++) {
				var item = $scope.recipient[i];
				
				if (i > 0) {
					result = result + ',';
				}
				result = result + item.email;				  
			 }	
			 
			 return result;
		 } 
		 
		 return $scope.recipient.email;
	 }
	 
	 init();
	 
 }]) 
 .controller('loginController', ['$scope', function ($scope) {
	 function init(){
		 $scope.isLoginInputFocused = true;
	 }
	 
	 init();
 }])
 .directive('focusMe', function($timeout, $parse) {
  return {
    //scope: true,   // optionally create a child scope
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) { 
          $timeout(function() {
            element[0].focus(); 
          });
        }
      });
      // to address @blesh's comment, set attribute value to 'false'
      // on blur event:
      element.bind('blur', function() {
         scope.$apply(model.assign(scope, false));
      });
    }
  };
});