var Enumerable = require('linq'); 
var users = module.exports = [];
var counter = 1;
function add(displayName, userName, password, url, phone, email, canCall, canSendEmail ){
	users.push({
		id: counter ++,
		displayName: displayName,
		userName: userName, 
		password: password, 
		url: url,
		phone: phone,
		email: email,
		canCall: canCall,
		canSendEmail: canSendEmail
	});
}

add('Администратор', 'root', 'root', '/images/root.jpeg', '102', 'khan.yuriy@gmail.com', true, true);
add('Ал-фон', 'alphone', 'alphone', '/images/alphone-user.png', '101', 'info@alphone.ru', true, true);
add('Без почты', 'noemail', 'noemail', '/images/root.jpeg', '102', null, true, false);

users.fetchById = function (id) {
	return Enumerable.from(users).firstOrDefault(function (user) {
		return user.id === id;
	});
}

users.fetch = function (userName, password) {
	return Enumerable.from(users).firstOrDefault(function (user) {
		return user.userName === userName && user.password === password;
	});
};