var data = module.exports = [];
	
function add(name, room, phone, email, url) {
	data.push({
		name: name, 
		room: room, 
		phone: phone,		
		email: email,
		url: url
	});
}

//add('geek-works', '102', '+7-926-652-89-58', 'khan.yuriy@gmail.com',  'images/root.jpg');	
// add('geek-works2', '105', '+7-926-652-89-58', 'khan.yuriy2@gmail.com',  'images/root.jpg');	
add('ООО «Сан Принт»', '104', '+7-495-626-42-43', 'info@sanprint.ru',  'images/sunprint.jpg');
add('ООО «Идеал Прайс»', '213', '+7-495-215-26-62', 'support@idealprice.ru',  'images/ideal-price.png');
add('ООО «Бьюти Стайл»', '103', '+7-495-419-96-96', 'yigal.rambam@gmail.com',  'images/fresh-mania.jpg');
add('ООО «Ди Эм Си»', '519', '+7-499-281-57-75', 'info@d-m-c.ru',  'images/dmc.png');
add('ЗАО «ЦСР»', '414', '+7-495-785-00-25', 'info@zaocsr.com',  'images/csr.png');
add('ООО «АЛ-ФОН»', '403', '+7-495-647-06-54', 'info@alphone.ru',  'images/alphone.png');
add('ООО «ТЕКА РУС»', '417-418', '+7-495-645-00-64', 'info@tekarus.ru',  'images/teka.jpg');