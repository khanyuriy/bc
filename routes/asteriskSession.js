var net = require('net');
var Packet = require('./packet.js');
var config = require('../config');

var actionCounter = 1;

function AsteriskSession() {
	this.socket = new net.Socket();
	this.socket.setEncoding('utf8'); 
	
	this.actionQueue = []; /* Очередь для действий, переданных на выполнения в момент авторизации */
	this.activeActions = Object.create(null); /* Активные действия */
	this.isConnecting = false;
	this.isConnected = false;
	this.isLoggedIn = false;
	
	this.socket.on('data', onData.bind(this));
	this.socket.on('error', onError.bind(this));
	this.socket.on('end', onEnd.bind(this));
	this.socket.on('close', onClose.bind(this));
}

var dataPacket = '';
function onData(data) {
	// Пакет может приходить частями. Собираем пакет до получения строки, заканчивающейся на \r\n\r\n 
	dataPacket = dataPacket+data;
	if (endsWith(data, '\r\n\r\n')){
		data = dataPacket;
		dataPacket = '';
	} else {
		return;
	}
	
	console.log('data= [\r\n'+data+']');
	var packet = Packet.deserialize(data);
	
	var actionId = packet.getActionId(); 
	
	// Залогинились
	if (actionId == 0) {
		var response = packet.getResponse();
		if (response == 'success') {
			logIn.call(this);			
		}
	} else {
		if (actionId in this.activeActions) {
			var action = this.activeActions[actionId];
			if (action.respond(this.socket, packet)){
				// Если команда завершила свое выполнение удалим ее
				delete this.activeActions[actionId]; 
			}
		}
	}	
}

function endsWith(str, suffix){
	 return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function logIn() {
	this.isLoggedIn = true;		
	console.log('logged in');
	
	/* Обработка очереди команд */
	while (this.actionQueue.length > 0) {
		var action = this.actionQueue.pop();
		this._coreDo(action);
	}		
}

function onError(err) {
	if (this.isConnecting) this.isConnecting = false;
	console.log(err);
}

function onEnd() {
	this.isConnected = false;
	this.isLoggedIn = false;
	console.log('end');	
}

function onClose() {
	this.isConnected = false;
	this.isLoggedIn = false;
	console.log('closed');	
}

AsteriskSession.prototype.start = function (port, host) {
	if (this.isConnecting || this.isConnected) { return; }
	this.port = port;
	this.host = host;
	
	var userName = config.asterisk.userName;
	var password = config.asterisk.password;
	
	this.isConnecting = true;
	var that = this;
	console.log('starting session');
	console.log('connecting to '+host+' on '+port+' as '+userName);
	
	this.socket.connect(port, host, function () {
		that.isConnecting = false;
		that.isConnected = true;
		
		var loginPacket = createLoginPacket(userName, password);
		
		that.socket.write(
			Packet.serialize(loginPacket)
		);
	});
};

AsteriskSession.prototype.do = function (action) {
	if (this.isLoggedIn === false) {
		
		if (action.isQueueable) {
			console.log('queueing action');
			this.actionQueue.unshift(action);	
		}
		
		if (this.isConnected === false) {
			console.log('reconnecting');
			this.start(this.port, this.host);
		}	
		 
	} else 
	{
		console.log('starting action');
		this._coreDo(action);		
	}	
};

AsteriskSession.prototype._coreDo = function (action) {
	var actionId = actionCounter ++;
	this.activeActions[actionId] = action;
	action.setActionId(actionId);
	action.execute(this.socket);
};

function createLoginPacket (userName, password) {
	var packet = new Packet(null);
	packet.set('Action', 'Login');
	packet.set('Username', userName);
	packet.set('Secret', password);
	packet.set('Events', 'off');
	packet.set('ActionID', '0');
	return packet;
};

module.exports = AsteriskSession;