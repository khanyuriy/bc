var Packet = require('./packet');

function LogoffAction() {
}

// свойство позволяет добавлять действие в очередь когда выполнение невозможно
LogoffAction.prototype.isQueueable = false;

LogoffAction.prototype.setActionId = function (actionId) {
	this.actionId = actionId;
};

LogoffAction.prototype.execute = function(socket) {
	
	var packet = new Packet(null);
	packet.set('Action', 'Logoff');
	packet.set('ActionID', this.actionId);
	
	console.log('send logof action: ');
	console.log(Packet.serialize(packet));
	socket.write(Packet.serialize(packet));
};

LogoffAction.prototype.respond = function(socket, packet) {
	return true;
};

module.exports = LogoffAction;