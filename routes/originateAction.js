var Packet = require('./packet');

function OriginateAction(from, to, res) {
	this.from = from;
	this.to = to;
	this.res = res;
}

// свойство позволяет добавлять действие в очередь когда выполнение невозможно
OriginateAction.prototype.isQueueable = false;

OriginateAction.prototype.setActionId = function (actionId) {
	this.actionId = actionId;
};

OriginateAction.prototype.execute = function(socket) {
	console.log('making originate from '+this.from+ ' to '+this.to);
	
	var packet = new Packet(null);
	packet.set('Action', 'Originate');
	packet.set('Channel', 'SIP/' + this.from);
	packet.set('CallerID', 'SIP/' + this.from);
	packet.set('Exten', 'SIP/VIVALDI/' + this.to);
	packet.set('Timeout', '15000');
	packet.set('Async', 'yes');
	packet.set('ActionID', this.actionId);
	
	console.log('send originate action: ');
	console.log(Packet.serialize(packet));
	socket.write(Packet.serialize(packet));
};

OriginateAction.prototype.respond = function(socket, packet) {
	var response = packet.getResponse();
	if (response == 'success'){
		console.log('originate success');
		this.res.end();
		return true;	
	}
	
	return false;
};

module.exports = OriginateAction;