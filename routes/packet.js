var Packet = function(data) {
	this.data = data || {};		
};

Packet.deserialize = function(asteriskResponse) {
	
	var data = Object.create(null);
		
	if (asteriskResponse == null || 
		(asteriskResponse.indexOf('Action:') < 0 &&
		asteriskResponse.indexOf('Event:') < 0 &&
		asteriskResponse.indexOf('Message:') < 0 &&
		asteriskResponse.indexOf('Response:') < 0) )
	{
		return new Packet(null);
	}
	
	var parts = asteriskResponse ? asteriskResponse.split('\r\n') : [];
	for (var i=0; i<parts.length; i++) {
		var part = parts[i];
		var els = part.split(':');
		if (els[0] && els[1]) {
			var key = els[0].trim();
			var value = els[1].trim().toLowerCase(); 
			data[key] = value;
		}
	}
	
	return new Packet(data);
};

Packet.serialize = function (packet) {
	var result = '';
	for (var key in packet.data) {
		result += key+':'+packet.data[key]+'\r\n';
	}
	result += '\r\n';
	
	return result;
};

Packet.prototype.getActionId = function() {
	return this.data.ActionID;	
};

Packet.prototype.getResponse = function () {
	return this.data.Response;	
};

Packet.prototype.getMessage = function () {
	return this.data.Message;	
};

Packet.prototype.toString = function () {
	return JSON.stringify(this.data);
};

Packet.prototype.set = function (key, value) {
	this.data[key] = value;	
};

Packet.prototype.isEmpty = function () {
	return Object.keys(this.data).length == 0;
};

module.exports = Packet;