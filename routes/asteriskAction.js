var Packet = require('./packet.js');

function AsteriskAction() {	
	this.packet = new Packet(null);
}

AsteriskAction.prototype.execute = function (socket) {
	socket.write(this.packet);
};

AsteriskAction.prototype.respond = function (socket, packet) {
	return true; // Результат означает что выполнение команды завершено. False значит что комманда ещё ожидает данных от Астериска	
};

module.exports = AsteriskAction;