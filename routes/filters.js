var filters = module.exports = {};

filters.authorize = function authorize(req, res, next) {
	if (req.isAuthenticated()) {
		next();
	} else 
	{
		if (req.is('json')) {
			res.status('401');
			res.end();
		} else {
			res.redirect('/login');
		}
	}
};