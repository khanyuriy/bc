var express = require('express');
var config = require('../config');
var Packet = require('./packet');
var Session = require('./asteriskSession')
var Originate = require('./originateAction');
var Logoff = require('./logoffAction');

var router = new express.Router();
var actionIdCounter = 1; 
var activeRequests = {};
var isAsteriskEnabled = config.asterisk.enabled;

if (isAsteriskEnabled === true){
	var session = new Session();
	session.start(config.asterisk.port, config.asterisk.host);
}

router.post('/call', function (req, res) {	
	req.resume();
	
	if (isAsteriskEnabled){
		var to = req.body.phone.replace(/\D/g, '');
		var originate = new Originate(req.user.phone, to, res);		
		session.do(originate);
	} else {
		console.log('Asterisk is disabled');
		res.end();
	}
}); 


module.exports = router;