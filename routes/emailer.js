var nodemailer = require('nodemailer');
var config = require('../config');

var transporter = nodemailer.createTransport(config.email);

var send = function (from, fromDisplayName, to, subject, text, successCallback, errorCallback) {
	var letter = {
	    from: fromDisplayName +' <'+from+'>', 
	    to: to, 
	    subject: subject, 
	    text: text 
	};
	
	console.log('sending email '+ JSON.stringify(letter));
	
	transporter.sendMail(letter, function onSendMail(error, info) {
		if (error) {
			errorCallback(error);
		} else {
			successCallback(info);	
		}		
	})
};

module.exports = send;