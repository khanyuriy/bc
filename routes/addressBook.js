/* global passport */
/// <reference path="../typings/node/node.d.ts"/>
var express = require('express');
var path = require('path');
var passport = require('passport');
var filter = require('./filters');
var companies = require('../data/companies');
var sendEmail = require('./emailer');


var router = new express.Router();
router.get('/', filter.authorize, function (req, res) {
	res.render('addressBook', {user: req.user, 
		angularApp: 'addressBookApp', angularCtl: 'addressBookController'});
	res.end();
});


router.get('/login', function (req, res) {
	res.render('login');
	res.end();
});

router.get('/logout', function (req, res) {
	req.logOut();
	res.redirect('/login');
});

router.post('/login', 
	passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/login'
	}), 
	function (req, res, next) {
		console.log('login post '+JSON.stringify(req.user));
		next();
	});

router.post('/companies', filter.authorize, function onCompanies(req, res) {
	res.json(companies);
	res.end();
});

router.get('/emailDialogTemplate', filter.authorize, function (req, res) {
	res.render('emailDialog');
	res.end();
});

router.post('/email', filter.authorize, function onEmail(req, res) {
	if (!req.user.canSendEmail) {
		res.status(401).end();
		return;
	}
	
	sendEmail(req.user.email, req.user.displayName, req.body.to, req.body.subject, req.body.text, function onSendSuccess(info) {
			console.log('sent mail: '+JSON.stringify(req.body));
			res.status('200').end();
		}, 
		function onSendError(error) {
			console.log('send mail error: '+JSON.stringify(error));
			res.status('500').end();
		});
});



module.exports = router;