/* global passport */
/// <reference path="typings/node/node.d.ts"/>
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var LocalStrategy = require('passport-local').Strategy;
var passport = require('passport');
var users = require('./data/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.locals.pretty = true;

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieSession({keys: ['key1', 'key2']}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

// Настройка passport
passport.use('local', new LocalStrategy(
  function (userName, password, done) {
    var user = users.fetch(userName, password);
    if (user) {
      return done(null, user);
    }    
    return done(null, false);
  }
));

passport.serializeUser(function onSerializeUser(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function onDeserializeUser(id, done) {
  var user = users.fetchById(id);
  if (user) {
    done(null, user);    
  } else {
    done(null, false);
  }
});

// Настройка маршрутов
var addressBookRoutes = require(path.join(__dirname, 'routes/addressBook'));
app.use('/', addressBookRoutes);

var asteriskRoutes = require(path.join(__dirname, 'routes/asterisk'));
app.use('/asterisk', asteriskRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
