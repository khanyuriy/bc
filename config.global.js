var conf = module.exports = {};

conf.DEVELOPMENT_ENV = 'development';
conf.RELEASE_ENV = 'release';

conf.port = 3000;

conf.asterisk = {
	enabled: true
};

conf.email = {
	service: 'gmail',
	auth: {
		user: 'mail.sender.bc@gmail.com',
		pass: 'gcP@ssword'
	}	
};

conf.env = conf.DEVELOPMENT_ENV;